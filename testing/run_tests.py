# -*- coding: utf-8 -*-
# ==============================================================================
"""
author
======

Novimir Antoniuk Pablant
- npablant@pppl.gov
- novimir.pablant@amicitas.com
"""
# ==============================================================================

from memory_profiler import profile, memory_usage
import argparse
import logging

# This allows testing to be done by running python tests/examples.py from the
# top level project directory.
import sys, os
sys.path.append(".")
os.environ['IDL_PATH'] = '+testing/:'+os.environ['IDL_PATH']

m_log = logging.getLogger('run_tests')

def test_01(use_idlrpc=False):

    print('\n'+'-'*80)
    print('Starting test_01')
    
    # Import mirpyidl.
    if use_idlrpc:
        import mirpyidlrpc as idl
    else:
        import mirpyidl as idl

    # Execute a command in IDL.
    # This will print 'Hello mom!' in the idlrpc window.
    # TEST_01 is the default IDL example for creating error handlers.
    # it will generate and recover from an error.  The end result
    # should be successful execution.
    idl.execute("TEST_01")
    
    m_log.info('test_01 sucessful.')


@profile
def test_02(use_idlrpc=False):
    """
    Check to see if variable dereferencing and memory allocation is 
    done correctly in both IDL and Python.

    In this test we copy data to and from Python and make sure we
    can free it in the expected manner.

    Warning:
      When IDL/Python frees a variable, it does not always release the memory
      back to the system.  If the allocated memory is big enough it
      should release it, but for smaller memory blocks it will use it
      internally.

      This effect may cause this test to fail.  In that case I probably just
      need to allocate bigger arrays.
    """
        
    print('\n'+'-'*80)
    print('Starting test_02')
    
    import numpy as np
    import sys, gc
    
    if use_idlrpc:
        import mirpyidlrpc as mirpyidl
    else:
        import mirpyidl

    idl = mirpyidl.PyIDL()

    
    memory_start = memory_usage()[0]
    
    # Create a numpy array in python.
    py_array_1 = np.random.normal(size=int(5e7))
    py_array_2 = np.random.normal(size=int(5e7))
    m_log.info('py_array_2.shape: {}'.format(py_array_2.shape))


    idl.ex("PRINT, 'IDL MEMORY USAGE (MiB):', MEMORY(/CURRENT)/1024E^2")
    
    # Copy the array to IDL.
    idl.setVariable('idl_array', py_array_2)
    idl.ex("PRINT, 'IDL MEMORY USAGE (MiB):', MEMORY(/CURRENT)/1024E^2")
    
    # Copy it back to Python
    py_array_3 = idl.getVariable('idl_array')
    #print('py_array_3.shape: {}'.format(py_array_3.shape))

    m_log.info('refcount py_array_1: {}'.format(sys.getrefcount(py_array_1)))
    m_log.info('refcount py_array_1: {}'.format(sys.getrefcount(py_array_1)))
    m_log.info('refcount py_array_1: {}'.format(sys.getrefcount(py_array_1)))
        
    # Now delete the Python variables.
    del py_array_1  
    del py_array_2
    del py_array_3
    
    # Finally delete the IDL array.
    idl.ex("PRINT, 'IDL MEMORY USAGE (MiB):', MEMORY(/CURRENT)/1024E^2")    
    idl.ex('DELVAR, idl_array')
    m_log.info('IDL: idl_array deleted')
    idl.ex("PRINT, 'IDL MEMORY USAGE (MiB):', MEMORY(/CURRENT)/1024E^2")

    memory_end = memory_usage()[0]
    memory_not_released = memory_end - memory_start
    m_log.info('Memory not released (MiB): {}'.format(memory_not_released))

    assert memory_not_released < 10.0, 'More than 10 MiB of memory has not be released.'

    m_log.info('test_02 sucessful.')

    
@profile
def test_03(use_idlrpc=False):
    """
    Check to see if we can allocate and release memory within IDL.
    In this test we do not copy any data over to Python.

    Warning:
      When IDL frees a variable, it does not always release the memory
      back to the system.  If the allocated memory is big enough it
      sholud release it, but for smaller memory blocks it will use it
      internally.

      This effect may cause this test to fail.  In that case I probably just
      need to allocate bigger arrays.
    """

    print('\n'+'-'*80)
    print('Starting test_03')
    
    import numpy as np
    import sys, gc
    
    if use_idlrpc:
        import mirpyidlrpc as mirpyidl
    else:
        import mirpyidl

    idl = mirpyidl.PyIDL()

    memory_start = memory_usage()[0]
    
    idl.ex("PRINT, 'IDL MEMORY USAGE (MiB):', MEMORY(/CURRENT)/1024E^2")
    m_log.info('Creating temp_01')
    idl.ex("temp_01 = LONARR(5E7)")
    idl.ex("PRINT, 'IDL MEMORY USAGE (MiB):', MEMORY(/CURRENT)/1024E^2")
    idl.ex('DELVAR, temp_01')
    m_log.info('IDL: temp_01 deleted')
    idl.ex("PRINT, 'IDL MEMORY USAGE (MiB):', MEMORY(/CURRENT)/1024E^2")

    m_log.info('Creating temp_02')
    idl.ex("temp_02 = LONARR(5E7)")
    idl.ex("PRINT, 'IDL MEMORY USAGE (MiB):', MEMORY(/CURRENT)/1024E^2")
    idl.ex('DELVAR, temp_02')
    m_log.info('IDL: temp_02 deleted')
    idl.ex("PRINT, 'IDL MEMORY USAGE (MiB):', MEMORY(/CURRENT)/1024E^2")

    memory_end = memory_usage()[0]
    memory_not_released = memory_end - memory_start
    m_log.info('Memory not released (MiB): {}'.format(memory_not_released))

    assert memory_not_released < 10.0, 'More than 10 MiB of memory has not be released.'

    m_log.info('test_03 sucessful.')


def test_04(use_idlrpc=False):
    """
    Attempt to set and restore a string.
    """

    print('\n'+'-'*80)
    print('Starting test_04')

    # Import mirpyidl.
    if use_idlrpc:
        import mirpyidlrpc as idl
    else:
        import mirpyidl as idl

    # Transfer a string to and from IDL
    string_in = 'This is a test string.'
    idl.setVariable("string_test", string_in)
    string_out = idl.getVariable("string_test")

    assert string_in == string_out, 'Retrieved string did not match input string.'

    m_log.info('test_04 sucessful.')

    
def test_05(use_idlrpc=False):
    """
    Attempt to retrieve a structure.
    """

    print('\n'+'-'*80)
    print('Starting test_05')

    # Import mirpyidl.
    if use_idlrpc:
        import mirpyidlrpc as idl
    else:
        import mirpyidl as idl

    idl.ex('test_structure = {yo:"mama", weight:1D6}')

    out = idl.getVariable("test_structure")

    assert out['YO'] == 'mama', 'Retrieved string did not match input string.'
    assert out['WEIGHT'] == 1e6, 'Retrieved value did not match input value.'
    
    m_log.info('test_05 sucessful.')



def test_06(use_idlrpc=False):
    """
    Attempt to retrieve a structure.
    """

    print('\n'+'-'*80)
    print('Starting test_06')

    # Import mirpyidl.
    if use_idlrpc:
        import mirpyidlrpc as idl
    else:
        import mirpyidl as idl

    idl.ex('temp = INDGEN(1,2,3,4,5,6,7,8)')
    temp = idl.getVariable('temp')

    idl.setVariable('temp2', temp)
    idl.ex('temp3 = temp2[0,0,0,0,0,0,3,0]')
    temp3 = idl.getVariable('temp3')

    assert temp[0,3,0,0,0,0,0,0] == 2160, 'Array transfer to Python not successful.'
    assert temp3 == 2160, 'Array transfer to IDL not successful.'

    m_log.info('test_06 sucessful.')


def test_initialization(use_idlrpc=False):
    """
    Check that only the appropriate backend was used.

    This should be run after all the other tests are complete.
    """
        
    print('\n'+'-'*80)
    print('Starting test_initialization')
    
    import mirpyidl
        
    if use_idlrpc:
        assert mirpyidl.m_idlrpc_initialized == True, 'IDLRPC was not initialized.'
        assert mirpyidl.m_idl_initialized == False, 'Callable IDL was initialized when only IDLRPC sholud have been used.'
    else:
        assert mirpyidl.m_idl_initialized == True, 'Callable IDL was not initialized.'
        assert mirpyidl.m_idlrpc_initialized == False, 'IDLRPC was initialized when only Callable IDL sholud have been used.'

    m_log.info('test_initializaiton sucessful.')

    
def setupLogging(debug=False):
    # Setup the logging output.
    if debug:
        level = logging.DEBUG
    else:
        level = logging.INFO
        
    logging.basicConfig(
        format='[%(asctime)s %(levelname)-6.06s:%(name)-15.015s]  %(message)s'
        ,datefmt= '%H:%M:%S'
        ,level=level)
    
    # Import mirpyidl.
    if args.idlrpc:
        import mirpyidlrpc as mirpyidl
    else:
        import mirpyidl

    if args.debug:
        mirpyidl.setLoggingLevel(level)

        
def run(use_idlrpc=False):
       
    # Run all the tests using CallableID
    if use_idlrpc:
        m_log.info('\n\nRunning mirpyidlrpc tests.')
    else:
        m_log.info('\n\nRunning mirpyidl tests.')
        

    # Run all the tests
    test_01(use_idlrpc)
    test_02(use_idlrpc)
    test_03(use_idlrpc)
    test_04(use_idlrpc)
    test_05(use_idlrpc)
    test_06(use_idlrpc)
    test_initialization(use_idlrpc)
     
    
if __name__ == '__main__':

    parser = argparse.ArgumentParser("""Run all the examples for mirpyidl""")
  
    parser.add_argument('--idlrpc'
                        ,action='store_true'
                        ,help="Use idlrpc for all tests.")
    parser.add_argument('--debug'
                        ,action='store_true'
                        ,help="Turn on debugging output.") 
    
    args = parser.parse_args()

    setupLogging(args.debug)

    # Run the tests.
    run(args.idlrpc)

